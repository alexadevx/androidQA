package com.erassytech.androidqa

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_open.*


class OpenActivity : AppCompatActivity() {
    private val KEY_TEXT_VALUE = "textValue"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sessionId = intent.getStringExtra("EXTRA_SESSION_ID")
        setContentView(R.layout.activity_open)
        myTextView.text = sessionId
        editText.setText("email")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        textView2.text ="onSaveInstanceState"
    }
}
