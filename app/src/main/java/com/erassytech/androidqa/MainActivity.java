package com.erassytech.androidqa;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goTo(View v) {
        String topic = v.getTag().toString();
        if(topic.equals("SPANNABLE"))
        {
            startActivity(new Intent(this, Spannable.class));
            //finish();
        }
        else if(topic.equals("INTENT"))
        {
            Intent intent = new Intent(this, OpenActivity.class);
            intent.putExtra("EXTRA_SESSION_ID", "AndroidQA");
            startActivity(intent);
        }

    }
}
