package com.erassytech.androidqa

import android.R.color
import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Typeface.BOLD
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
import android.text.style.BulletSpan
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_spannable.*


class Spannable : AppCompatActivity() {
    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = "Spannable"
        setContentView(R.layout.activity_spannable)

        val string = SpannableString("Text with \n formatting spannable")
        string.setSpan(
                StyleSpan(BOLD),
                8, string.length,
                SPAN_EXCLUSIVE_EXCLUSIVE)
        string.setSpan(
                ForegroundColorSpan(Color.RED),
                8, string.length,
                SPAN_EXCLUSIVE_EXCLUSIVE)

        myTextView.text = string
    }
}