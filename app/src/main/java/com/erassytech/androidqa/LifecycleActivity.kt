package com.sam_chordas.android.androidlifecycle

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity

import com.erassytech.androidqa.R

class LifecycleActivity : AppCompatActivity() {
    protected override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.i(LOG_TAG, "LifecycleActivity onCreate")

    }

    protected override fun onStop() {
        super.onStop()
        Log.i(LOG_TAG, "LifecycleActivity onStop")
    }

    protected override fun onDestroy() {
        super.onDestroy()
        Log.i(LOG_TAG, "LifecycleActivity onDestroy")
    }

    protected override fun onPause() {
        super.onPause()
        Log.i(LOG_TAG, "LifecycleActivity onPause")
    }

    protected override fun onResume() {
        super.onResume()
        Log.i(LOG_TAG, "LifecycleActivity onResume")
    }

    protected override fun onStart() {
        super.onStart()
        Log.i(LOG_TAG, "LifecycleActivity onStart")
    }

    companion object {
        val LOG_TAG = LifecycleActivity::class.java.simpleName
    }
}